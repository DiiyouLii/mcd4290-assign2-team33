// Code for the View Run page.

// The following is sample code to demonstrate navigation.
// You need not use it for final app.

var runIndex = localStorage.getItem(APP_PREFIX + "-selectedRun");
var runIndex_2 = JSON.parse(localStorage.getItem(APP_PREFIX + "-selectedRun"));


function deleteRun()
{ 
    for(var i =0; i<runIndex_2.length;i++)
    {
        if(runIndex._endDate===runIndex_2[i]._endDate)
            
            {
            localStorage.removeItem(APP_PREFIX + "-selectedRun");
            }
    }
    location.href = "index.html";
}

if (runIndex !== null)
{
    // If a run index was specified, show name in header bar title. This
    // is just to demonstrate navigation.  You should set the page header bar
    // title to an appropriate description of the run being displayed.
    var runNames = [ "heading"];
    document.getElementById("headerBarTitle").textContent = runNames[runIndex];
}
