// Code for the Measure Run page.

/*
Task 2
/*
find:
+ Latitude Location Value
+ Longitude Location Value
+ Accurate Location ( combine Long and Lat)
+ Current Location
*/

var currentLocation; 
var longiLocation;
var latiLocation;
var accurateLocation;
var map;
function showingLocation(){
mapboxgl.accessToken='pk.eyJ1IjoidG5ndTAxMTIiLCJhIjoiY2thOWxraDcxMDg4ejJ5bnRkcGc2c2Z1YyJ9.IeBfPyzAYLG2yHutenwN_A';

map = new mapboxgl.Map
    ({
    container: 'map',
    style: 'mapbox://styles/mapbox/streets-v11',
    zoom: 17,
    center: [-55,37.8]  
    }); 

navigator.geolocation.watchPosition(currentPosition,fixingErrors)
}

// Making callback function for currentPosition

function currentPosition(actualPosition) 
{
    // Shows current lat and long of user
    // Calculation  longi, lati and accurateLocation
    
    longiLocation=Number(actualPosition.coords.longitude)
    latiLocation=Number(actualPosition.coords.latitude)
    accurateLocation=Number(actualPosition.coords.accuracy)
    
    // Combine Long and Lat and display
    
    currentLocation = [longiLocation,latiLocation]
    console.log(currentLocation)
    
    //Create marker and popup 
    
    let popup= new mapboxgl.Popup({ offset: 35 })
        .setText("Present Position")
        .addTo(map)

     let marker= new mapboxgl.Marker({color: "#00CED1"})
        .setLngLat(currentLocation)
        .addTo(map)
        .setPopup(popup);
    
    map.panTo(currentLocation)
      
    if (accurateLocation <= 2000)
    { document.getElementById('randomDestination').disabled=false; }

else
    {   alert("Error during Loading")
        document.getElementById("randomDestination").disabled=true; }
}

  function fixingErrors(debugging)
{
    if      (debugging.code ===1)  {alert("Location cannot load")}
    else if (debugging.code ===2)  {alert("Unspecified Location")}
    else if (debugging.code ===3)  {alert("Time out")}
    else                           {alert("Error during loading")}
}
                                                                                    


//Task 3:
// Calculate the random value of longi and lati
// Change the longi and lati to Radian
//

var Coordinates; 

var longi1; //Old
var longi2; //New

var lati1;  //Old
var lati2;  //New

var delLongi; // 
var delLati; //

var calBracket1;
var calBracket2;

var radiusOfEarth = 6372;

var checkDistance;

var marker2;
var popup2;

function randomPosition(Coordinates)
{
    //Random coordinate of longi and lati
    
    randomLongi = ((-0.001+0.002*Math.random())+longiLocation);
    randomLati = ((-0.001+0.002*Math.random())+latiLocation);
}

function distanceOfTwoPoints()
{
        //calculate value for longi and lati (old and new)
        //longitude and delta distance
    
        longi1 = longiLocation*Math.PI/180;
        longi2 = randomLongi*Math.PI/180;
        delLongi = (randomLongi-longiLocation)*Math.PI/180;
        
    
        //latitude and delta distance
    
        lati1 = latiLocation*Math.PI/180;
        lati2 = randomLati*Math.PI/180;
        delLati = (randomLati-latiLocation)*Math.PI/180;
          
    
        calBracket1 = Math.sin(delLati/2) * Math.sin(delLati/2)         
                    + Math.cos(lati1) * Math.cos(lati2) 
                    * Math.sin(delLongi/2) * Math.sin(delLongi/2)
        
        calBracket2 = 2 * Math.atan2 (Math.sqrt(calBracket1), Math.sqrt(1-calBracket1))
    
    distanceOf2Points =  radiusOfEarth * calBracket2; 
    
return distanceOf2Points
    
    
}


function display(){
    if (distanceOf2Points >= 60 && distanceOf2Points <= 150)
        {document.getElementById('runningPath').disabled=false}
    
    else {document.getElementById('runningPath').disabled=true}
    
return display
}

//Create function random destination ;
function randomDestination()
{
    //callback 2 functions
    
    randomPosition();
    distanceOfTwoPoints();
    
    // Make the condition true for the function
    
let conditionFunctions=true;
    
    if (marker2 != undefined)
        {
            if (popup2 != undefined)
            {
                marker2.remove();
                marker2 === undefined;
                    
                popup2.remove();
                popup2 === undefined;
            }       
        }
    
 
 // restrict minimum 60m and maximum 150m
    
    if (display = true)
    { 
        
        Coordinates=[randomLongi,randomLati];
    
        // create a new marker and popup for the final
    
        marker2=new mapboxgl.Marker({color:"gold"})
            .setLngLat(Coordinates)
            .addTo(map)
    
        popup2=new mapboxgl.Popup({offset:40})
            .setLngLat(Coordinates)
            .setText('Final Position')
            .addTo(map)
    
        map.panTo(currentLocation)
    
       
    }
    
    else 
        
    {
        
    // Repeating the process untill finish
    // Repeating the true statement        
    randomPosition();
    distanceOfTwoPoints();
    
}}
    
    
// Task 4
/*
Make function start run as start the time recording
Draw the Line up of from des1 to des 2 with geojson
*/

var arrayOfLocation=[];
var startDate; //Time

// Creating function for Starting run
function startRun()
{
    //set up beginning time
    startDate = new Date()

    function pushEleInArray()
    {
        // Push element for array 
        arrayOfLocation.push(currentLocation)
}}


//Task 5
/* 
Creating a function completed Run 
Stop time
display
*/

var endDate;
var timeRecording;
var roundCD;
var roundD;

//Creating function for Ending Run

function completedRun()
{   
    //set up ending Time
    endDate = new Date();
    
    checkDistance = distanceOfTwoPoints(longi1,lati1,longi2,lati2)
    roundCD = Math.round(checkDistance*1000)
    document.getElementById('checkDistance').innerHTML= 'Real Distance:' + roundCD + ' m';
    
    distance = distanceOfTwoPoints(currentLocation[0],currentLocation[1],Coordinates[0],Coordinates[1])
    roundD = Math.round(distance*1000)
    document.getElementById('distance').innerHTML= 'Travelling Distance:' + roundD +' m';
    
    timeRecording = endDate.getTime()-startDate.getTime();
    document.getElementById('timeRecording').innerHTML = 'Time Recored: ' + timeRecording/1000 + ' s';

}

//Task 6
/* 
Put name and save in localStorage
*/

var newRun;
var newRunJson;
var savedRun;
var savedRuns=[];

function saveRun(){
    
    if (typeof(locationstorage) !== 'null')
    { 
        newRun = new Run();
        newRun.setstartLocation (currentLocation);
        newRun.setdesLocation(Coordinates);
        newRun.setstartDate(startDate);
        
        newRun.setpathTaken(arrayOfLocation);
        newRun.setendDate(endDate);
        newRun.settimeRecording(timeRecording);
        newRun.setdistance(distance);
        savedRun = prompt ('Creating name for the Run ')
        
        savedRuns.push(newRun);
        
        newRunJson = JSON.stringify(newRun);
        localStorage.setItem(savedRun, newRunJson);    
        location.href = "index.html";
        
}}



//Task 7:
    var saved = [];
    var localSaved;
    function containPastRun()
{
	saved=[];
	localSaved=localStorage.getItem(APP_PREFIX+STORAGE_KEY);
	if(localSaved)
	{
		localSaved= JSON.parse(localSaved);
        
		for(var k=0 ; k<localSaved.length; k++)
		{
			saved[k]=newRun;
			saved[k].initialRun(localSaved[k]);
}}}
    


//Addition Function draw a line in a map
function runningPath(){
    
// using geojson to interact with map and create a line on map
    map.addSource(
        'path', 
        {
            'type': 'geojson',
            'data': 
        {
            'type': 'Feature',
            'properties': {},
            'geometry': 
        {
            'type': 'LineString',
            'coordinates':
        [
            currentLocation,
            Coordinates
]}}});
// Drawing a line, make color
    map.addLayer({
        
        'id': 'path',
        'source': 'path',
        'type': 'line',
        'paint': 
        {
            'line-color': "red",   
}});
    
 // display on map   
document.getElementById('startRun').disabled = false;
}
















