// Shared code needed by all three pages.

// Prefix to use for Local Storage.  You may change this.
var APP_PREFIX = "monash.mcd4290.runChallengeApp";
var STORAGE_KEY="Run";
// Array of saved Run objects.

class Run
{
	constructor(
	startLocation, desLocation, pathTaken, startDate, endDate,timeRecording,distance,heading)
	{
		this.startLocation= startLocation;
		this.desLocation= desLocation;
		this.pathTaken= pathTaken;
		//empty Array
		this.startDate= startDate;
		this.endDate= endDate;
        this.timeRecording= timeRecording;
        this.distance= distance;
	}


//Class for startLocation, desLocation, startDate for task 4
// Mutator: set, Accessor: get
//startLocation ==================================
    
setstartLocation (startLocation)
    {
        this.startLocation= startLocation;
    }
getstartLocation()
    {
        return this.startLocation
    }
    
// startDate ==================================
setstartDate (startDate)
    {
        this.startDate= startDate;
    }
getstartDate()
    {
        return this.startDate
    }

//desLocation ==================================
setdesLocation (desLocation)
    {
        this.desLocation= desLocation;
    }
getdesLocation()
    {
        return this.desLocation
    }
    
//Class for pathTaken, endDate, time recording and distance
    
//pathTaken ==================================
setpathTaken (pathTaken)
    {
        this.pathTaken= pathTaken;
    }
getpathTaken()
    {
        return this.pathTaken
    }

//endDate ==================================
setendDate (endDate)
    {
        this.endDate= endDate;
    }
getendDate()
    {
        return this.endDate
    }
    
//timeRecording ==================================
settimeRecording (timeRecording)
    {
        this.timeRecording= timeRecording;
    }
gettimeRecording()
    {
        return this.timeRecording
    }
    
//distance ==================================
setdistance (distance)
    {
        this.distance= distance;
    }
getdistance()
    {
        return this.distance
    }

    
	initialRun(runPoint){
		this._heading=runPoint._heading;
		this._startLocation=runPoint._startLocation;
		this._desLocation=runPoint._desLocation;
		this._pathTaken=runPoint._pathTaken;
		this._startDate=runPoint._startDate;
		this._endDate=runPoint._endDate;
		this._distance=runPoint._distance;
		this._timeRecording=runPoint._timeRecording;
		
	}
}

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
