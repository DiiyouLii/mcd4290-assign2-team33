// Code for the main app page (Past Runs list).

// The following is sample code to demonstrate navigation.
// You need not use it for final app.

var startLocation;
var desLocation;
var startDate= new Date();
var endDate= new Date();
var pathTaken;
var heading;

var displayInfomation="";

var localSaved=JSON.parse(localStorage.getItem(APP_PREFIX+"-selectedRun"));
var reference =document.getElementById("runsList");

function showInformation(runPoint)
{
	for (var i = 0; i < localSaved.length; i++) 
    {
        
    startLocation = runPoint[i]._startLocation;
    desLocation = runPoint[i]._desLocation;
    startDate = runPoint[i]._startDate;
    endDate = runPoint[i]._endDate;
    pathTaken = runPoint[i]._pathTaken;
	heading=runPoint[i]._heading;
    
    
    displayInfomation += "<li class=\"mdl-list__item mdl-list__item--two-line\" onclick=\"viewRun("+i+");\">"
    displayInfomation += "<span class=\"mdl-list__item-primary-content\">"
    displayInfomation += "<span>"+heading+"</span>"
    displayInfomation += "<span class=\"mdl-list__item-sub-title\">"+"Starting Location"+ startLocation+"</span>"+"<br>"
    displayInfomation += "<span class=\"mdl-list__item-sub-title\">"+"Ending Location"+ desLocation+"</span>"+"<br>"
    displayInfomation += "<span class=\"mdl-list__item-sub-title\">"+"Starting Time"+ startDate + "</span>"+"<br>"
    displayInfomation += "<span class=\"mdl-list__item-sub-title\">"+"Ending Time"+ endDate + "</span>"+"<br>"
    displayInfomation +="</span>"              
	displayInfomation +="</li>" 
        
	return displayInfomation;
    
    
    reference.innerHTML = displayInfomation(i);
}
}



function viewRun(runIndex)
{
    // Save the desired run to local storage so it can be accessed from View Run page.
    localStorage.setItem(APP_PREFIX + "-selectedRun", runIndex);
    // ... and load the View Run page.
    location.href = 'viewRun.html';
}


