// Code for the Measure Run page.

//Task 2
/* 
find:
+Latitude Location Value
+Longitude Location Value
+Accurate Location (combine Long and Lat)
+Current Location
*/

var currentLocation;
var longiLocation;
var latiLocation;
var accurateLocation;

// Find out current: Navigator & mapboxgl

navigator.geolocation.watchPosition(currentPosition,fixingError)

mapboxgl.accessToken = 'pk.eyJ1IjoidG5ndTAxMTIiLCJhIjoiY2s5cXh2b2xuMG9qNzNkbzM5cDRsd2ZtciJ9.cwBc5BD-zhdveZOBzfcr-Q';

// Making function calllback for currentPosition
function currentPosition (presentLocation)
{
     // Making value for long, lat and current Location
    
    longiLocation=Number(presentLocation.coords.longitude).toFixed(4)
    latiLocation=Number(presentLocation.coords.latitude).toFixed(4)
    accurateLocation=Number(presentLocation.coords.accuracy).toFixed(4)
    
    //Combine  Long and Lat to display
    
    currentLocation = [longiLocation,latiLocation];
    console.log(currentLocation)
    
    //Creating location mark from week 5 practical
    
    let map = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/mapbox/streets-v10',
            zoom: 16,
            center: currentLocation
        });
    
    let marker = new mapboxgl.Marker()
        marker.setLngLat(currentLocation)
        marker.addTo(map);
    
    let popup = new mapboxgl.Popup ({ offset:40})
        popup.setText("Present Location");
        marker.setPopup(popup)
    //Pan to map of currentLoction
        map.panTo(currentLocation);   
}

    // Making a callback function for fixingError

function fixingError(debugging)
{
    if      (debugging.code ===1)  {alert("Location cannot load")}
    else if (debugging.code ===2)  {alert("Unspecified Location")}
    else if (debugging.code ===3)  {alert("Time out")}
    else                           {alert("Error during loading")}
}















