// Shared code needed by all three pages.

// Prefix to use for Local Storage.  You may change this.
var APP_PREFIX = "monash.mcd4290.runChallengeApp";

// Array of saved Run objects.
var savedRuns = [];
class Run
{
	constructor(
	startLocation, desLocation, locArray, startDate, endDate)
	{
		this.startLocation= startLocation;
		this.desLocation= desLocation;
		this.pathTaken= locArray;
		//empty Array
		this.startDate= startDate;
		this.endDate= endDate;
	}
}
